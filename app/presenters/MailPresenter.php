<?php
use Nette\Application\UI\Form;
/**
 * Presenter for editing mails such as newUserMail, resetPasswordMail and registrationMail.
 * @author tomasprix
 */
class MailPresenter extends BasePresenter {
    private $mailRepository;
    
    /**
     * (non-phpDoc)
     *
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->isLogged(1);
        $this->isAdmin(1);
        
        $this->mailRepository = $this->context->mailRepository;
    }

    /**
     * Will redirect to renderList()
     */
    public function actionDefault() {
        $this->redirect('Mail:list');
    }

    /**
     * Will render Mail list
     */
    public function renderList($pagin = 1) {
        $count = $this->mailRepository->countAll();
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($count); // number of entries
        $paginator->setItemsPerPage(5); // entries per page
        $paginator->setPage($pagin?:1); 
        
        $this->template->paginator = $paginator;
        $this->template->mailList = $this->mailRepository->findAll() ->limit($paginator->getLength(), $paginator->getOffset());
    }
    
    /**
     * Call form for editing Mail entry 
     * @param $id
     * @param $pagin
     */
    public function renderEdit($id = 0, $pagin = 1) {
        $form = $this['addForm'];
        if (!$form->isSubmitted()) {
            $mailInstance = $this->mailRepository->findById($id);
            if (!$mailInstance) {
                $this->error($this->translate('Row was not found.', 'alert-error'));
            }
            $form->setDefaults($mailInstance);
            $form->setDefaults(array('pagin' => $pagin));
        }
    }
    
    /**
     * Call form for editing Mail entry 
     */
    protected function createComponentAddForm(){
        $form = new Form;
        $form->addHidden('pagin');
        $form->addText('subject', 'Subject:',100)
                ->setRequired('Subject was not filled in!');
        $form->addText('salut', 'Salut:',50)
                ->setRequired('Salut was not filled in!');
        $form->addTextArea('text', 'Text:', 8, 10)
                ->setRequired('Text was not filled in!');
        $form->addText('regards', 'Regards:',50)
                ->setRequired('Regards was not filled in!');
        $form->addSubmit('save', 'Save')
                ->setAttribute('kind', 'mainButtonNear')         
                ->onClick[] = $this->memberFormSucceeded;
        $form->addSubmit('cancel', 'Back')
                ->setValidationScope(NULL)
                ->onClick[] = $this->formCancelled;
        $form->addProtection();
        return $form;
    }
    
    /**
     * Handler for form canceling.
     */
    public function formCancelled($button){
        $pagin = $button->getForm()->getValues()->pagin;
        $this->redirect('Mail:list', array('pagin'=>$pagin));
    }
    
    /**
     * Handler for editing mails in database.
     * @param array $button'
     */
    public function memberFormSucceeded($button){
        $values = $button->getForm()->getValues();
        $id = (int) $this->getParameter('id');
        if ($id) { //EDITING
            try {
                $mailInstance = $this->mailRepository->findById($id);
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
            }
            
            try {
                $mailInstance->subject=$values->subject;
                $mailInstance->salut=$values->salut;
                $mailInstance->text=$values->text;
                $mailInstance->regards=$values->regards;
                $mailInstance->update();
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error!', 'alert-error'));
                $this->redirect('Mail:list');
            }

            $this->flashMessage($this->translate('Changes in entry ').$mailInstance->mailID.$this->translate(' was successfully saved.'), 'alert-success');
        }  
        $this->redirect('Mail:list', array('pagin'=>$values->pagin));
    }

    /**
     * Will render mail preview
     */
    public function renderPreview($id = 0, $pagin = 1){
        $mailInstance = $this->mailRepository->findById($id);
        if (!$mailInstance) {
            $this->error($this->translate('Row was not found.', 'alert-error'));
        }
        $this->template->pagin = $pagin;
        $this->template->mailInstance = $this->mailRepository->findByID($id);
    }

}