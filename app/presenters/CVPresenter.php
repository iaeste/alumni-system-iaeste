<?php
use Nette\Application\UI\Form;
/**
 * Presenter for editing cv.
 * @author tomasprix
 */
class CVPresenter extends BasePresenter {
    private $textRepository;
    private $cvRepository;
    private $userRepository;
    /**
     * (non-phpDoc)
     *
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->isLogged(1);
        
        $this->textRepository = $this->context->textRepository;
        $this->cvRepository = $this->context->cvRepository;
        $this->userRepository = $this->context->userRepository;
    }
    
    /**
     * Call form for editing CV entry 
     */
    public function renderEdit() {
        $this->isLogged(1);
        $form = $this['addForm'];
        $cvInstance = $this->cvRepository->findById($this->userRepository->findById($this->getUser()->getIdentity()->id)->cv_id);
        
        if ($cvInstance) {
            $form->setDefaults(array(
                'text' => $cvInstance->cvText
            ));
        }
    }
    
    /**
     * Factory for CV entry 
     */
    protected function createComponentAddForm(){
        $form = new Form;
        $form->addTextArea('text', 'Text:', 8, 10)
                ->setAttribute('kind', 'texyla') 
                ->getControlPrototype()->id("texyla");
        $form->addSubmit('save', 'Save and preview')
                ->setAttribute('kind', 'mainButtonFar')         
                ->onClick[] = $this->addFormSucceeded;
        $form->addProtection();
        return $form;
    }
    
    /**
     * Handler for editing CV in database.
     * @param array $button'
     */
    public function addFormSucceeded($button){
        $this->isLogged(1);
        $values = $button->getForm()->getValues();
        $id = (int) $this->getUser()->getIdentity()->cv_id;
        if ($id) { //EDITING
            try {
                $cvInstance = $this->textRepository->findById($id);
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Row was not found!'), 'alert-error');
            }
            
            try {
                $cvInstance->cvText=$values->text;
                $cvInstance->update();
                $this->flashMessage($this->translate('Changes in CV successfully saved. '), 'alert-success');
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error!'), 'alert-error');
                $this->redirect('Homepage:');
            }

            
        }else{
            try{
                //creating cv instance
                $cvInstance = $this->cvRepository->insert(array(
                    'cvText' => $values->text
                ));
                
                //saving id of cv instance in user
                $userInstance = $this->userRepository->findById($this->getUser()->getIdentity()->id);
                $userInstance->cv_id = $cvInstance->id;
                $userInstance->update();
                $this->flashMessage($this->translate("CV successfully created."), 'alert-success');
           } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error!'), 'alert-error');
                $this->redirect('Homepage:');
           }
            
            
            
        }  
        
        $this->redirect('CV:preview');
    }

    /**
     * Will render CV preview
     */
    public function renderPreview(){
        $this->isLogged(1);
        $cvInstance = $this->cvRepository->findById($this->userRepository->findById($this->getUser()->getIdentity()->id)->cv_id);
        if (!$cvInstance) {
            $this->error($this->translate('Row was not found.'), 'alert-error');
        }
        $this->template->cvInstance = $cvInstance;
    }

}