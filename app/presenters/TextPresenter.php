<?php
use Nette\Application\UI\Form;
/**
 * Presenter for editing texts such as homepage, userInfo and aminInfo.
 * @author tomasprix
 */
class TextPresenter extends BasePresenter {
    private $textRepository;
    /**
     * (non-phpDoc)
     *
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->isLogged(1);
        $this->isAdmin(1);
        
        $this->textRepository = $this->context->textRepository;
    }

    /**
     * Will redirect to renderList()
     */
    public function actionDefault() {
        $this->redirect('Text:list');
    }

    /**
     * Will render Text list
     */
    public function renderList($pagin = 1) {
        $count = $this->textRepository->countAll();
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($count); // number of entries
        $paginator->setItemsPerPage(5); // entries per page
        $paginator->setPage($pagin?:1); 
        
        $this->template->paginator = $paginator;
        $this->template->textList = $this->textRepository->findAll() ->limit($paginator->getLength(), $paginator->getOffset());
    }
    
    /**
     * Call form for editing Text entry 
     * @param $id
     */
    public function renderEdit($id = 0, $pagin = 1) {
        $form = $this['addForm'];
        if (!$form->isSubmitted()) {
            $textInstance = $this->textRepository->findById($id);
            if (!$textInstance) {
                $this->error($this->translate('Row was not found.', 'alert-error'));
            }
            $form->setDefaults($textInstance);
            $form->setDefaults(array('pagin' => $pagin));
        }
    }
    
    /**
     * Call form for editing Text entry 
     */
    protected function createComponentAddForm(){
        $form = new Form;
        $form->addHidden('pagin');
        $form->addText('name', 'Name:',30)
                ->setRequired('Name was not filled in!');
        $form->addTextArea('text', 'Text:', 8, 10)
                ->setAttribute('kind', 'texyla') 
                ->getControlPrototype()->id("texyla");
        $form->addSubmit('save', 'Save and preview')
                ->setAttribute('kind', 'mainButtonFar')         
                ->onClick[] = $this->addFormSucceeded;
        $form->addSubmit('cancel', 'Back to list')
                ->setValidationScope(NULL)
                ->onClick[] = $this->formCancelled;
        $form->addProtection();
        return $form;
    }
    
    /**
     * Handler for form canceling.
     */
    public function formCancelled($button){
        $pagin = $button->getForm()->getValues()->pagin;
        $this->redirect('Text:list', array('pagin'=>$pagin));
    }
    
    /**
     * Handler for editing text in database.
     * @param array $button'
     */
    public function addFormSucceeded($button){
        $values = $button->getForm()->getValues();
        $id = (int) $this->getParameter('id');
        if ($id) { //EDITING
            try {
                $textInstance = $this->textRepository->findById($id);
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
            }
            
            try {
                $textInstance->name=$values->name;
                $textInstance->text=$values->text;
                $textInstance->update();
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error!', 'alert-error'));
                $this->redirect('Text:list');
            }

            $this->flashMessage($this->translate('Changes in entry ').$textInstance->textID.$this->translate(' was successfully saved.'), 'alert-success');
        }  
        $this->redirect('Text:preview', array('id'=>$textInstance->id,'pagin'=>$values->pagin));
    }

    /**
     * Will render text preview
     */
    public function renderPreview($id = 0, $pagin = 1){
        $textInstance = $this->textRepository->findById($id);
        if (!$textInstance) {
            $this->error($this->translate('Row was not found.', 'alert-error'));
        }
        $this->template->pagin = $pagin;
        $this->template->textInstance = $this->textRepository->findByID($id);
    }

}