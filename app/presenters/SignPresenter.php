<?php

use Nette\Application\UI\Form;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter{
    private $userRepository;
    private $mailRepository;

    
    /**
     * (non-phpDoc)
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->userRepository = $this->context->userRepository;
        $this->mailRepository = $this->context->mailRepository;
    }
    
    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm(){
        $form = new Form();
        $form->setTranslator($this->translator);
        $form->addText('username', 'Username:', 30, 80)
                ->setRequired('Email wasnt filled!')
                ->addCondition(Form::FILLED)
                    ->addCondition(~Form::EQUAL, "admin")
                        ->addRule(Form::EMAIL, 'Wrong email format!');
        $form->addPassword('password', 'Password:', 30)
                ->setRequired('Password wasnt filled!');
        $form->addCheckbox('persistent', 'Remember me');
        $form->addSubmit('login', 'Login')
                ->setAttribute('kind', 'mainButtonNear')         
                ->onClick[] = $this->signInFormSubmitted;
        
        $form->addSubmit('forgottenPassword', 'Forgotten password')
                ->setValidationScope(NULL)
                ->onClick[] = $this->forgottenPassword;
        
        $form->addSubmit('about', 'About')
                ->setValidationScope(NULL)
                ->onClick[] = $this->about;
        return $form;
    }
    
    /**
     * Redirect for forgotten button fot Sign-in form
     */
    public function forgottenPassword(){
        $this->redirect('Sign:forgottenPassword');
    }
    
    /**
     * Redirect for about button fot Sign-in form
     */
    public function about(){
        $this->redirect('Homepage:about');
    }
    
    /**
     * Sign-in form handler. Will try to login user. Needs Authenticator.
     */
    public function signInFormSubmitted($button){
        $form = $button->getForm();
        $values = $form->getValues();

        if ($values->persistent) {
            $this->getUser()->setExpiration('+ 14 days', FALSE);
        } else {
            $this->getUser()->setExpiration('+ 20 minutes', TRUE);
        }

        try {
            $this->getUser()->login($values->username, $values->password);
        } catch (Nette\Security\AuthenticationException $e) {
            if($e->getCode()==4){//User is not activated
                $form->addError($this->translate("User is not activated!"));
                return;
            }else{//Wrong password or unregistered email address
                $form->addError($this->translate("Unregistered user or wrong password!"));
                return;
            }
            return;
        }

        if($this->getUser()->isInRole('admin')){
            $count = $this->userRepository->pendingUsersCount();
            if($count>0){
                $this->flashMessage($this->translate("There is $count pending user.", $count));
                $this->redirect('Users:list', array('usersFilter'=>"pending"));
            }
        }
        $this->redirect('Users:list');
    }
    
    /**
     * Handler for token recieving. If token is OK, then newPasswordForm factory is called.
     */
    public function renderNewPassword(){
        if($this->isLogged(0)){
            $this->getUser()->logout();
        }
        
        $token = $this->getParam('token');
        
        //token recieved?
        if (!$token){ 
            $this->redirect('Homepage:');
        }
        
        $userInstance = $this->userRepository->findByToken($token);
        
        //token found?
        if(!$userInstance){ 
            $this->flashMessage($this->translate('Token not found. Enter your email address for new, please.'), 'warning-error');
            $this->redirect('Sign:forgottenPassword');
        }
        
        //token expired?
        if($userInstance->tokenExp < new \Nette\DateTime){
            $this->flashMessage($this->translate('Token expired. Enter your email address for new, please.'), 'warning-error');
            $this->redirect('Sign:forgottenPassword');
        }
        
        $this->flashMessage($this->translate('Enter your new password please.'), 'warning');
        $form = $this['newPasswordForm'];
        if (!$form->isSubmitted()) {
           $form->setDefaults($userInstance);
        }
    }
    
    /**
     * Handler for changing user password when user is loggedin.
     */
    public function renderChangePassword(){
        $this->isLogged(1);
        $userInstance = $this->userRepository->findById($this->getUser()->getIdentity()->getId());

        $form = $this['newPasswordForm'];
        if (!$form->isSubmitted()) {
            $form['save']->caption = 'Save';
            $form->setDefaults($userInstance);
        }
    }

    /**
     * NewPassword form factory.
     */
    protected function createComponentNewPasswordForm(){
        $form = new Form();
        
        $form->setTranslator($this->translator);
        $form->addHidden('username');
        if($this->isLogged()){
            $form->addPassword('passwdOld', 'Old password:', 80, 20)
            ->setRequired('Old password must be filled in!')
            ->addCondition(Form::FILLED)
                ->addRule(Form::LENGTH, 'Password must have from %d to %d znaků!', array(6,20));
        }
        
        $form->addPassword('passwd1', 'New password:', 80, 20)
            ->setRequired('Password must be filled in!')
            ->addCondition(Form::FILLED)
                ->addRule(Form::LENGTH, 'Password must have from %d to %d znaků!', array(6,20));
        $form->addPassword('passwd2', 'Password check:', 80, 20)
            ->setRequired('Password check must be filled in!')
            ->addCondition(Form::FILLED)
                ->addRule(Form::EQUAL, 'Password check and Password must be same!', $form['passwd1']);
        
        
        $form->addSubmit('save', 'Save new password')
                ->setAttribute('kind', 'mainButtonNear');
        $form->onSuccess[] = $this->newPasswordFormSubmitted;
        return $form;
    }
    
    /**
     * Handler for newPasswordForm
     */
    public function newPasswordFormSubmitted(Form $form){
        $values = $form->getValues();
        $userInstance = $this->userRepository->findByUsername($values->username);
        
        if(!$userInstance){         
            $this->flashMessage($this->translate("User does not exist!"), "alert-error");
            $this->redirect('Homepage:');
        }
        
        if(!$this->isLogged()){
            //token expired?
            if($userInstance->tokenExp < new \Nette\DateTime){
                $this->flashMessage($this->translate('Token expired. Enter your email address for new, please.'), "alert-error");
                $this->redirect('Sign:forgottenPassword');
            }    
        }else{
            if($userInstance->password != Todo\Authenticator::calculateHash($values->passwdOld, $userInstance->password)){
                $form->addError($this->translate("Wrong old password!"));
                return;
            } 
        }

        //Try to save user password and log in
        try {
            $userInstance->token=null;
            $userInstance->tokenExp=null;
            $userInstance->password = \Todo\Authenticator::calculateHash($values->passwd1);
            $userInstance->update();
            $this->getUser()->login($values->username, $values->passwd1);
        } catch (Exception $e) {
            $this->flashMessage($this->translate("Unexpected error!"), "alert-error");
            $this->redirect('Homepage:');
        }
        
        $this->flashMessage($this->translate('Password successfully changed.'), "alert-info");
        $this->redirect('Users:list');
    }
    
    protected function createComponentForgottenPasswordForm(){
        $form = new Form();
        
        $form->setTranslator($this->translator);
        $form->addText('username', 'Email:', 30, 80)
                ->setRequired('Email wasnt filled!')
                ->addCondition(Form::FILLED)
                    ->addCondition(~Form::EQUAL, "admin")
                        ->addRule(Form::EMAIL, 'Wrong email format!');
        $form->addSubmit('send', 'Send new password')
                ->setAttribute('kind', 'mainButtonNear');;
        $form->onSuccess[] = $this->resetPasswordFormSubmitted;
        return $form;
    }
    
    public function resetPasswordFormSubmitted(Form $form){        
        $values = $form->getValues();

        $userInstance = $this->userRepository->findByUsername($values->username);
        
        if(!$userInstance){         
            $form->addError($this->translate("Unregistered email address entered!"));
            return;
        }
        $token = Todo\Authenticator::calculateToken();
        $time = new Nette\DateTime();
        $time->add(new DateInterval('PT2H')); //Two hours added

        try {
            $userInstance->token=$token;
            $userInstance->tokenExp= $time;
            $userInstance->password= Todo\Authenticator::calculateHash(uniqid());
            $userInstance->update();
        } catch (Exception $e) {
            $this->flashMessage($this->translate("Unexpected error!"));
            return;
        }
        $mail=$values->username=="admin"?$this->context->constantRepository->findByConstantID('adminEmail')->constant:$values->username;
        $this->sendMail('resetPasswordMail', $this->link('//Sign:newPassword', array('token'=>$token)), $mail);         
        
        $this->flashMessage($this->translate("Email send to address $mail.", "alert-info"));
        $this->redirect('Homepage:about');
    }

    public function actionOut(){
        $this->getUser()->logout();
        $this->flashMessage($this->translate('You have been logged out.'), "alert-info");
        $this->redirect('in');
    }
}
