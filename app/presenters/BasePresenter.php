<?php
use \Todo\mailerControl;
use Nette\Utils\Strings; 
use \Texy;
/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter{
    /** @persistent */
    public $lang = "cs";
    
    /** @var string @persistent */
    public $ajax = 'off';

    /** @var bool */
    public $oldLayoutMode = false;

    /** @var NetteTranslator\Gettext */
    protected $translator;
    
    private $texy;
    /**
     * Inject translator
     * @param NetteTranslator\Gettext
     */
    public function injectTranslator(NetteTranslator\Gettext $translator){
        $this->translator = $translator;
    }


    public function createTemplate($class = NULL){
        $template = parent::createTemplate($class);

        // pokud není nastaven, použijeme defaultní z configu
        if (!isset($this->lang)) {
            $this->lang = $this->context->parameters["lang"];
        }
        
        $texy = new Texy();
        // disable *** and ** and * phrases
        $texy->allowed['phrase/strong+em'] = FALSE;
        $texy->allowed['phrase/strong'] = FALSE;
        $texy->allowed['phrase/em-alt'] = FALSE;
        $texy->allowed['phrase/em-alt2'] = FALSE;
        
        // allow only this html tags
        $texy->allowedTags = array(
            'h3' => Texy::NONE,  
            'h4' => Texy::NONE, 
            'h5' => Texy::NONE, 
            'div' => Texy::ALL,  
            'p' => Texy::NONE, 
            'b' => Texy::NONE, 
            'i' => Texy::NONE, 
            'ul' => Texy::NONE, 
            'ol' => Texy::NONE, 
            'li' => Texy::NONE, 
            'img' => Texy::ALL, 
            'table' => Texy::NONE, 
            'td' => Texy::NONE, 
            'tr' => Texy::NONE, 
            'th' => Texy::NONE, 
            'caption' => Texy::NONE, 
            'tbody' => Texy::NONE, 
            'thead' => Texy::NONE, 
            'tfoot' => Texy::NONE, 
            'a' => array('href', 'lang', 'target'), 
        );
        
        // disable *** and ** and * phrases
        $texy->allowed['phrase/strong+em'] = FALSE;
        $texy->allowed['phrase/strong'] = FALSE;
        $texy->allowed['phrase/em-alt'] = FALSE;
        $texy->allowed['phrase/em-alt2'] = FALSE;
        
        
        // add new syntax: .h2 ...
        $texy->registerBlockPattern(
                $this->userBlockHandler,
                '#^(h2)\.(.+)$#m',
                'myBlockSyntax1'
        );
        
        // add new syntax: .h3 ...
        $texy->registerBlockPattern(
                $this->userBlockHandler,
                '#^(h3)\.(.+)$#m',
                'myBlockSyntax2'
        );
        
        // add new syntax: .h4 ...
        $texy->registerBlockPattern(
                $this->userBlockHandler,
                '#^(h4)\.(.+)$#m',
                'myBlockSyntax3'
        );
        
        // add new syntax: _italic_
        $texy->registerLinePattern(
                $this->userInlineHandler,
                '#(?<!_)_(?!\ |_)(.+)'.TEXY_MODIFIER.'?(?<!\ |_)_(?!_)()#U',
                'myInlineSyntax2'
        );

        // add new syntax: *bold*
        $texy->registerLinePattern(
                $this->userInlineHandler,  // callback function or method
                '#(?<!\*)\*(?!\ |\*)(.+)'.TEXY_MODIFIER.'?(?<!\ |\*)\*(?!\*)()#U', // regular expression
                'myInlineSyntax1' // any syntax name
        );
        $this->texy = $texy;

        
        $template->registerHelper('texy', callback($texy, 'process'));

        $this->translator->setLang($this->lang); // nastavíme jazyk
        $template->setTranslator($this->translator);

        return $template;
    }
    
    public function handleTexyRewrite($text){
        $template = new Nette\Templating\FileTemplate('toTexy.latte');
        $template->text = $text;
	$template->render();
        $this->invalidateControl();
        //$text = $template;
        //$this->payload->text = $this->template;
        
        $this->sendPayload();
        //$this->sendResponse(new JsonResponse($text|texy));
    }
    
    
    /**
    * Pattern handler for block syntaxes
    *
    * @param TexyBlockParser
    * @param array      regexp matches
    * @param string     pattern name (myBlockSyntax1)
    * @return TexyHtml|string|FALSE
    */
   public function userBlockHandler($parser, $matches, $name)
   {
           list(, $mTag, $mText) = $matches;

           $texy = $parser->getTexy();

           // create element
           if ($mTag === 'perex') {
                   $el = TexyHtml::el('div');
                   $el->attrs['class'][] = 'perex';

           } else {
                   $el = TexyHtml::el($mTag);
           }

           // create content
           $el->parseLine($texy, $mText);

           return $el;
   }
   
   /**
 * Pattern handler for inline syntaxes
 *
 * @param TexyLineParser
 * @param array   reg-exp matches
 * @param string  pattern name (myInlineSyntax1 or myInlineSyntax2)
 * @return TexyHtml|string
 */
public function userInlineHandler($parser, $matches, $name)
{
	list(, $mContent, $mMod) = $matches;

	$texy = $parser->getTexy();

	// create element
	$tag = $name === 'myInlineSyntax1' ? 'b' : 'i';
	$el = TexyHtml::el($tag);

	// apply modifier
	$mod = new TexyModifier($mMod);
	$mod->decorate($texy, $el);

	$el->attrs['class'] = 'myclass';
	$el->setText($mContent);

	// parse inner content of this element
	$parser->again = TRUE;

	return $el;
}
    

    public function translate($message, $count = 1){
        $message = $this->translator->translate($message, $count);
        return $message;
    }
    
    /**
     * Will return Navbar component
     * @return Nette\Application\UI\Control
     */
    public function createComponentNavbar(){
        return new Todo\NavbarControl($this->translator, $this->lang, $this->context->languageRepository->findAll());
    }
    
    /**
     * Will return Footer component
     * @return Nette\Application\UI\Control
     */
    public function createComponentFooter(){
        return new Todo\FooterControl($this->translator);
    }
    
    /**
     * If user is logged in return true othervise redirect to Homepage
     * @param boolean (0 - without redirect, 1 - with redirect)
     * @return boolean 
     */
    public function isLogged($withRedirect=0){
        if (!$this->getUser()->isLoggedIn()) {
            if($withRedirect==1){
                $this->flashMessage($this->translate('You must be logged in for access!', 'error'));
                $this->redirect('Sign:in');
            }
            return false;
        }
        return true;
    }
    
    /**
     * If user role is admin return true othervise redirect to Homepage
     * @param boolean (0 - without redirect, 1 - with redirect)
     * @return boolean
     */
    public function isAdmin($withRedirect=0){
        if (!$this->getUser()->isInRole('admin')) {
            if($withRedirect==1){
                $this->flashMessage($this->translate('You must have administrator role for access!', 'error'));
                $this->redirect('Homepage:default');
            }
            return false;
        }
        return true;
    }   
    
    /**
     * Caller for component Mailer
     * @param String $emailID
     * @param String $emailLink
     * @param String $to (if to is not filled in mail will be sent to default reciever/sender)
     */
    public function sendMail($emailID, $emailLink = "", $to = null){
        $constant = $this->context->constantRepository; 
        $mail = new mailerControl($constant->findByConstantID('adminEmail')->constant, $this->textToArray($constant->findByConstantID('contact')->constant));    
        $mail ->sendMail($this->context->mailRepository->findByMailID($emailID), $emailLink, $to);
    }
    
    /**
     * Caller for component Mailer
     * @param String $from (if to is not filled in mail will be sent to default reciever/sender)
     * @param String $text
     * @param String $subject 
     */
    public function sendUserMail($from, $text, $subject){
        $constant = $this->context->constantRepository; 
        $mail = new mailerControl($constant->findByConstantID('adminEmail')->constant, $this->textToArray($constant->findByConstantID('contact')->constant));
        $mail ->sendUserMail($from, $text, $subject);    
    }
    
    /**
    * Handler for spliting String into Array.
    * @param String $operation
    * @return array $id
    */
    public function textToArray($text){
        return Strings::split($text, '~;\s*~');
    }
    
    /**
    * Handler for operations.
    * @param String $operation
    * @param array $id
    */
    public function gridOperationsHandler($operation, $id){
        $row = implode(', ', $id);
        $this->redirect($operation, array('id' => $row));
    }

    /**
     * Texyla loader factory
     * @return TexylaLoader
     */
    protected function createComponentTexyla(){
        $baseUri = $this->context->httpRequest->url->baseUrl;
        $filter = new WebLoader\Filter\VariablesFilter(array(
            "baseUri" => $baseUri,
            "previewPath" => $this->link("Texyla:preview"),
            "filesPath" => $this->link("Texyla:listFiles"),
            "filesUploadPath" => $this->link("Texyla:upload"),
            "filesMkDirPath" => $this->link("Texyla:mkDir"),
            "filesRenamePath" => $this->link("Texyla:rename"),
            "filesDeletePath" => $this->link("Texyla:delete"),
        ));

        $texyla = new TexylaLoader($filter, $baseUri."webtemp");
        return $texyla;
    }
    
}
    
