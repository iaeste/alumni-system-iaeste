<?php
use Nette\Application\UI\Form;
/**
 * Presenter for manipulation of LC (Add, edit, remove, liat).
 * @author tomasprix
 */
class LcPresenter extends BasePresenter {
    private $lcRepository;
    
    /**
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->isLogged(1);
        $this->isAdmin(1);
        
        $this->lcRepository = $this->context->lcRepository;
    }

    /**
     * Will redirect to renderList()
     */
    public function actionDefault() {
        $this->redirect('Lc:list');
    }

    /**
     * Will render Lc list
     */
    public function renderList($pagin) {
        $count = $this->lcRepository->countAll();
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($count); // number of entries
        $paginator->setItemsPerPage(5); // entries per page
        $paginator->setPage($pagin?:1); 
        
        $this->template->paginator = $paginator;
        //$this->template->lang = $this->lang;
        $this->template->lcList = $this->lcRepository->findAll() ->limit($paginator->getLength(), $paginator->getOffset());
    }

    /**
     * Call form for adding LC
     */
    public function renderAdd($pagin = 1) {
        $this['addForm'];
        $this->template->pagin = $pagin;
        return;
    }

    /**
     * Will permanently delete LC entry
     * @param $id
     */
    public function actionDelete($id = 0) {
        $pagin = (int) $this->getParameter('pagin');
        $count = $this->context->userRepository->numberOfUsersWithLcID($id);
        $lc = $this->lcRepository->findById($id)->lcName;
        if($count>0){
            $this->flashMessage($this->translate("Cannot be deleted. There is $count user assigned to $lc.", $count), 'alert-error');
        }else{
            $m = $this->lcRepository->delete($id);
            if($m == true){
                $this->flashMessage($this->translate('Row successfully removed.'), 'alert-success');
            } else {
                $this->flashMessage($this->translate('Row was not found.'), 'alert-error');
            }
        }
        $this->redirect('Lc:list', array('pagin'=>$pagin));
    }

    /**
     * Call form for editing LC entry 
     * @param $id
     */
    public function renderEdit($id = 0, $pagin = 1) {
        $form = $this['addForm'];
        if (!$form->isSubmitted()) {
            $lcInstance = $this->lcRepository->findById($id);
            if (!$lcInstance) {
                $this->error($this->translate('Row was not found.', 'alert-error'));
            }
            $form['save']->caption = 'Save';    //Change submit Add to Save
            $form->setDefaults($lcInstance);
            $form->setDefaults(array('pagin' => $pagin));
        }
    }
    
    /**
     * Call form for editing LC entry 
     */
    protected function createComponentAddForm(){
        $form = new Form;
        $form->addHidden('pagin');
        $form->addText('lcName', 'Name:',50)
                ->setRequired('Name was not filled in!');
        
        $form->addSubmit('save', 'Add')
                ->setAttribute('kind', 'mainButtonNear')         
                ->onClick[] = $this->addFormSucceeded;
        $form->addSubmit('cancel', 'Back')
                ->setValidationScope(NULL)
                ->onClick[] = $this->formCancelled;

        $form->addProtection();
        return $form;
    }

    /**
     * Handler for form canceling.
     */
    public function formCancelled($button){
        $pagin = $button->getForm()->getValues()->pagin;
        $this->redirect('Lc:list', array('pagin'=>$pagin));
    }
    
    /**
     * Handler for inserting/editing lc in database.
     * @param array $button'
     */
    public function addFormSucceeded($button){
        $values = $button->getForm()->getValues();
        $id = (int) $this->getParameter('id');
        if ($id) { //EDITING
            try {
                $lcInstance = $this->lcRepository->findById($id);
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
            }
            
            try {
                $lcInstance->lcName=$values->lcName;
                $lcInstance->update();
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error!', 'alert-error'));
                $this->redirect('Lc:list');
            }

            $this->flashMessage($this->translate('Changes in entry ').$lcInstance->lcName.$this->translate(' was successfully saved.'), 'alert-success');
        } else { //CREATING
            $lcInstance = $this->lcRepository->insert(array(
                    'lcName' => $values->lcName
            ));
            
            $this->flashMessage($this->translate('Lc ').$lcInstance->lcName.($this->translate(' added.')), 'alert-success');
        }   
        $this->redirect('Lc:list', array('pagin'=>$values->pagin));
    }
}