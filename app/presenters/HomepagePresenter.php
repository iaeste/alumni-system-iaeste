<?php
use Nette\Application\UI\Form;
/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter{
    private $textRepository;
    
    protected function startup(){
        parent::startup();

        $this->textRepository = $this->context->textRepository;
    }

    public function actionDefault(){
        //$this->redirect('Sign:in');
        $this->redirect('Homepage:about');
    }
    
    public function renderAbout(){
        $this->template->textInstance = $this->textRepository->findByTextID('homepage');
        
        $this['sendEmailForm'];
    }
    
    public function renderContactUs(){
        $this['sendEmailForm'];
    }
    
    public function renderInfo(){
        $this->isLogged(1);
        if($this->isAdmin()){
            $this->template->textInstance = $this->textRepository->findByTextID('adminInfo');
        }else{
            $this->template->textInstance = $this->textRepository->findByTextID('userInfo');
        }
    }
    
    /**
     * Factory for sending email form
     */
    protected function createComponentSendEmailForm(){
        $form = new Form;
        if(!$this->isLogged()){
            $form->addText('contact', 'My email:',100)
                ->setAttribute("required", "true")
                ->setRequired('Your email must be filled in!');
        }
        $form->addText('subject', 'Subject:',50)
                ->setAttribute("required", "true")
                ->setRequired('Subject was not filled in!');
        $form->addTextArea('text', 'Text:', 8, 10)
                ->setAttribute("required", "true")
                ->setRequired('Text was not filled in!')
                ->addRule(Form::MAX_LENGTH, 'Email is too long', 4000);
        $form->addSubmit('send', 'Send')
                ->setAttribute('kind', 'mainButtonNear')         
                ->onClick[] = $this->sendEmailSucceeded;
        $form->addProtection();
        return $form;
    }
    
    
    /**
     * Handler for editing mails in database.
     * @param array $button'
     */
    public function sendEmailSucceeded($button){
        $values = $button->getForm()->getValues();
        
        if($this->isLogged()){
            $values->contact = $this->getUser()->getIdentity()->username;
        }
        
        $this->sendUserMail($values->contact, $values->text, $values->subject);
        
        $this->flashMessage($this->translate('Email send to administrator.'), 'alert-success');

        $this->redirect('Homepage:');
    }
}
