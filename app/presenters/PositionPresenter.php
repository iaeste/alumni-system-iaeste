<?php
use Nette\Application\UI\Form;
/**
 * Presenter for manipulation of Position (Add, edit, remove, liat).
 * @author tomasprix
 */
class PositionPresenter extends BasePresenter {
    private $positionRepository;
    
    /**
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->isLogged(1);
        $this->isAdmin(1);
        
        $this->positionRepository = $this->context->positionRepository;
    }

    /**
     * Will redirect to renderList()
     */
    public function actionDefault() {
        $this->redirect('Position:list');
    }

    /**
     * Will render Position list
     */
    public function renderList($pagin) {
        $count = $this->positionRepository->countAll();
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($count); // number of entries
        $paginator->setItemsPerPage(5); // entries per page
        $paginator->setPage($pagin?:1); 
        
        $this->template->paginator = $paginator;
        //$this->template->lang = $this->lang;
        $this->template->positionList = $this->positionRepository->findAll() ->limit($paginator->getLength(), $paginator->getOffset());
    }

    /**
     * Call form for adding Position
     */
    public function renderAdd($pagin = 1) {
        $this['addForm'];
        $this->template->pagin = $pagin;
        return;
    }

    /**
     * Will permanently delete Position entry
     * @param $id
     */
    public function actionDelete($id = 0) {
        $pagin = (int) $this->getParameter('pagin');
        $count = $this->context->userRepository->numberOfUsersWithpositionID($id);
        $position = $this->positionRepository->findById($id)->positionName;
        if($count>0){
            $this->flashMessage($this->translate("Cannot be deleted. There is $count user assigned to $position.", $count), 'alert-error');
        }else{
            $m = $this->positionRepository->delete($id);

            if($m == true){
                $this->flashMessage($this->translate('Row successfully removed.', 'alert-success'));
            } else {
                $this->flashMessage($this->translate('Row was not found.', 'alert-error'));
            }
        }
        $this->redirect('Position:list', array('pagin'=>$pagin));
    }

    /**
     * Call form for editing Position entry 
     * @param $id
     */
    public function renderEdit($id = 0, $pagin = 1) {
        $form = $this['addForm'];
        if (!$form->isSubmitted()) {
            $positionInstance = $this->positionRepository->findById($id);
            if (!$positionInstance) {
                $this->error($this->translate('Row was not found.', 'alert-error'));
            }
            $form['save']->caption = 'Save';    //Change submit Add to Save
            $form->setDefaults($positionInstance);
            $form->setDefaults(array('pagin' => $pagin));
        }
    }
    
    /**
     * Call form for editing Position entry 
     */
    protected function createComponentAddForm(){
        $form = new Form;
        $form->addHidden('pagin');
        $form->addText('positionName', 'Name:',50)
                ->setRequired('Name was not filled in!');
        
        $form->addSubmit('save', 'Add')
                ->setAttribute('kind', 'mainButtonNear')         
                ->onClick[] = $this->addFormSucceeded;
        $form->addSubmit('cancel', 'Back')
                ->setValidationScope(NULL)
                ->onClick[] = $this->formCancelled;

        $form->addProtection();
        return $form;
    }

    /**
     * Handler for form canceling.
     */
    public function formCancelled($button){
        $pagin = $button->getForm()->getValues()->pagin;
        $this->redirect('Position:list', array('pagin'=>$pagin));
    }
    
    /**
     * Handler for inserting/editing position in database.
     * @param array $button'
     */
    public function addFormSucceeded($button){
        $values = $button->getForm()->getValues();
        $id = (int) $this->getParameter('id');
        if ($id) { //EDITING
            try {
                $positionInstance = $this->positionRepository->findById($id);
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
            }
            
            try {
                $positionInstance->positionName=$values->positionName;
                $positionInstance->update();
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error!', 'alert-error'));
                $this->redirect('Position:list');
            }

            $this->flashMessage($this->translate('Changes in entry ').$positionInstance->positionName.$this->translate(' was successfully saved.'), 'alert-success');
        } else { //CREATING
            $positionInstance = $this->positionRepository->insert(array(
                    'positionName' => $values->positionName
            ));
            
            $this->flashMessage($this->translate("Position $positionInstance->positionName added."), 'alert-success');
        }   
        $this->redirect('Position:list', array('pagin'=>$values->pagin));
    }
}