<?php
use Nette\Application\UI\Form;
use Grido\Grid,
    Grido\Components\Filters\Filter,
    Grido\Components\Columns\Column,
    Nette\Utils\Html;
use Nette\Utils\Validators;
use Nette\Utils\Strings;
use Nette\Http\Url;

use Nette\Image;


/**
 * Presenter for listing users, user registration and editation, administrating of users. 
 * @author tomasprix
 */
class UsersPresenter extends BasePresenter {
    private $userRepository; 
    private $lcRepository; 
    private $mailRepository; 
    private $positionRepository; 
    private $usersFilter;
    /**
     * (non-phpDoc)
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        
        $this->userRepository = $this->context->userRepository;
        $this->lcRepository = $this->context->lcRepository;
        $this->positionRepository = $this->context->positionRepository;
        $this->mailRepository = $this->context->mailRepository;
        
        $this->usersFilter='active';
    }
    
    
    /**
     * Redirect to renderList
     */
    public function actionDefault() {
        $this->isLogged(1);
        $this->redirect('list');
    }
    
    /**
     * Render list of users
     * @param $usersFilter
     */
    public function renderList($usersFilter='active') {
        $this->isLogged(1);
        $this->usersFilter=$usersFilter;
        if($this->isAdmin()){
            if($this->usersFilter!=='active' && $this->usersFilter!=='banned' &&
               $this->usersFilter!=='pending' && $this->usersFilter!=='deleted' &&
               $this->usersFilter!=='all'
                    )$this->usersFilter='active';
            
        }else{
            if($this->usersFilter!='active') $this->usersFilter='active';
        }
        $this->template->usersFilter =  $this->usersFilter;
    }
    
    /**
     * Factory for grid
     */
    protected function createComponentGrid($name){       
        $grid = new Grid($this, $name);
        $grid->setModel($this->userRepository->joinWithPositionAndLC());
        
        
        //NAME
        $grid->addColumn('name', $this->translate("Name"), Column::TYPE_TEXT)
                ->setSortable()
                ->setFilterText()
                    ->setSuggestion();

        //SURNAME
        $grid->addColumn('surname', $this->translate("Surname"), Column::TYPE_TEXT)
                ->setSortable()
                ->setFilterText()
                    ->setSuggestion();
        
        //MAIL
        $grid->addColumnMail('username', $this->translate("Email"))
                ->setSortable()
                ->setFilterText()
                    ->setSuggestion();
        
        //MEMBER SINCE
        $grid->addColumnDate('memberSince', $this->translate("Since"), 'm.Y')
                ->setSortable()
                ->setFilterDate()
                    ->setCondition(Filter::CONDITION_CALLBACK, 
                            callback($this, 'gridMemberSinceFilterCondition'));
        $grid->getColumn('memberSince')->cellPrototype->class = "center";
        
        //ALUMNI SINCE
        $grid->addColumnDate('alumniSince', $this->translate("To"), 'm.Y')
                ->setCustomRender(function($item){
                    if($item->alumniSince!='-0001-11-30 00:00:00')return date_format($item->alumniSince, 'm.Y');
                    return '-';
                })
                ->setSortable()
                ->setFilterDate()
                    ->setCondition(Filter::CONDITION_CALLBACK, 
                            callback($this, 'gridAlumniSinceFilterCondition'));
        $grid->getColumn('alumniSince')->cellPrototype->class = "center";
        
        //CV (admin only)
        /*if($this->isAdmin()){
            $grid->addColumnText('allowedCV', 'CV')
                ->setReplacement(array(0 => 'N', 1=> 'A'))
                ->setFilterSelect(array('' => '', '0' => 'N', '1' => 'A'));

            $header = $grid->getColumn('allowedCV');
            $header->headerPrototype->style = 'width: 4%';
            $header->headerPrototype->class = "center";
            $header->cellPrototype->style= 'width: 4%';
            $header->cellPrototype->class = "center";
            
            
        }*/
        
        //LC
        $grid->addColumnText('lc_id', $this->translate("LC"))
                ->setColumn('lcName')
                ->setSortable()
                ->setReplacement(array('1'=>'pok'));
          
        $grid->addFilterSelect('lc_id', 'LC', 
                array('' => '') + $this->lcRepository->getArray('lcName')
                );
        
       //POSITION
        $grid->addColumnText('position_id', $this->translate("Position"))
                ->setColumn('positionName')
                ->setSortable();

        $grid->addFilterSelect('position_id', 'Position', 
                array('' => '') + $this->positionRepository->getArray('positionName')
                );
        
        //ACTIONS
        

        $grid->addActionHref('view', '', 'view', array('backlink' => $this->storeRequest()))
                    ->setIcon('eye-open');
        
        if($this->isAdmin()){
            switch ($this->usersFilter) {
            case 'active':
                $grid->addActionHref('edit', '', 'edit', array('backlink' => $this->storeRequest()))
                    ->setIcon('pencil');
                $grid->addActionHref('bann', '', 'bann', array('backlink' => $this->storeRequest()))
                    ->setIcon('remove')
                    ->setConfirm($this->translate("Are you sure you want to bann this user?"))
                    ->getElementPrototype()->class[] = 'btn-danger';
                $grid->addActionHref('delete', '', 'delete', array('backlink' => $this->storeRequest()))
                    ->setIcon('trash')
                    ->setConfirm($this->translate("Are you sure you want to delete this user?"))
                    ->getElementPrototype()->class[] = 'btn-danger';

                $operations = array('toMailString' => $this->translate('...to email address string'), 
                                    'bann' => $this->translate('...bann'),
                                    'delete' => $this->translate('...delete'));
                $grid->setOperations($operations, callback($this, 'gridOperationsHandler'))
                        ->setConfirm('delete', $this->translate('Are you sure you want to delete %i items?'));
                $grid->setExporting();   
                break;
            case 'banned':
                $grid->addActionHref('unbann', '', 'unbann', array('backlink' => $this->storeRequest()))
                    ->setIcon('ok-sign');

                //$operations = array('a' => $this->translate('...unbann'));
                //$grid->setOperations($operations, callback($this, 'gridOperationsHandler'));
                break;
            case 'pending':
                $grid->addActionHref('confirm', '', 'confirm', array('backlink' => $this->storeRequest()))
                    ->setIcon('ok');
                break;
            case 'deleted':
                $grid->addActionHref('unDelete', '', 'undelete', array('backlink' => $this->storeRequest()))
                    ->setIcon('circle-arrow-left');

                //$operations = array('unDelete' => $this->translate('...unDelete'));
                //$grid->setOperations($operations, callback($this, 'gridOperationsHandler'))
                //        ->setConfirm('unDelete', $this->translate("Are you sure you want to retrieve %i items?"));
                break;
            }
        }else{
            $operations = array('toMailString' => $this->translate('...to email address string'));
            $grid->setOperations($operations, callback($this, 'gridOperationsHandler'));
            $grid->setExporting();  
        }
        
        
        //SETTERS
        $grid->translator->setLang('cs');  
        $grid->setFilterRenderType(Filter::RENDER_INNER);   //filters are in table
                                   //
                      
        //USERS filter
        if($this->isAdmin()){
            $list = array('active' => $this->translate('Active users'),
                      'banned' => $this->translate('Banned users'),
                      'pending' => $this->translate('Pending users'),
                      'deleted' => $this->translate('Deleted users'),
                      'all' => $this->translate('All users'));
        
        
            $grid->addFilter('state', 'State', Filter::TYPE_SELECT, $list)
                ->setCondition(Filter::CONDITION_CUSTOM, array(
                    'active' => '[state] = 2',
                    'banned' => '[state] = 1',
                    'pending' => '[state] = 0',
                    'deleted' => '[state] = 3',
                    'all' => '1=1'
            ));
            
            if($this->usersFilter!=='active' && $this->usersFilter!=='banned' &&
               $this->usersFilter!=='pending' && $this->usersFilter!=='deleted' &&
               $this->usersFilter!=='all'
                    )$this->usersFilter='active';
        }else{
            $list = array('active' => $this->translate('Active users'));

            $grid->addFilter('state', 'State', Filter::TYPE_SELECT, $list)
                ->setCondition(Filter::CONDITION_CUSTOM, array(
                    'active' => '[state] = 2'
            ));
            if($this->usersFilter!='active') $this->usersFilter='active';
        }
        
        $grid->addColumnText('state', 'State');
        
        //will hide state column
        $state = $grid->getColumn('state');
        $state->headerPrototype->rowspan = "2";
        $state->headerPrototype->style = 'display: none;';
        $state->cellPrototype->style = 'display: none;';
        $grid->getFilter('state')->wrapperPrototype->style = 'display: none;';
        $grid->rememberState=false;
        $grid->setDefaultFilter(array('state' => $this->usersFilter));
    }
    
    /**
     * Custom condition callback for filter memberSince (more or equal).
     * @param string $value
     * @return array|NULL
     */
    
    public function gridMemberSinceFilterCondition($value){
        $date = explode('.', $value);
        foreach ($date as &$val) {
            $val = (int) $val;
        }

        return count($date) == 3
            ? array('[memberSince] >= %s', "{$date[2]}-{$date[1]}-01")
            : NULL;
    }
    
    /**
     * Custom condition callback for filter alumniSince (less or equal).
     * @param string $value
     * @return array|NULL
     */
    
    public function gridAlumniSinceFilterCondition($value){
        $date = explode('.', $value);
        foreach ($date as &$val) {
            $val = (int) $val;
        }

        return count($date) == 3
            ? array('[AlumniSince] <= %s', "{$date[2]}-{$date[1]}-01")
            : NULL;
    }
    
  
    /**
     * Render detail of user
     * @param int $id
     */
    public function renderView($backlink = '', $id = 0){
        $this->isLogged(1);
        $userInstance = $this->userRepository->findById($id);
        if (!$userInstance) {
            $this->error($this->translate('Entry was not found'));
        }
        $this->template->backlink = $backlink;
        $this->template->userInstance = $userInstance; 
    }
    
    /**
     * Handle returning from view
     * @param string $backlink
     */
    public function actionReturn($backlink = ''){
        $this->restoreRequest($backlink);
        $this->redirect('Users:list');
    }

    /**
     * Set user->state = 2 true and send email
     * @param $id
     */
    public function actionConfirm($backlink = '', $id = 0){
        $this->isLogged(1);
        $this->isAdmin(1);
        $userInstance = $this->userRepository->findById($id);
        if (!$userInstance) {
            $this->error($this->translate('Entry was not found'));
        }
        
        try {
            $time = new Nette\DateTime();
            $time->add(new DateInterval('PT12H'));
            $token = \Todo\Authenticator::calculateToken();
            
            $userInstance->state=2;
            $userInstance->token=$token;
            $userInstance->tokenExp=$time;
            $userInstance->password= Todo\Authenticator::calculateHash(uniqid());
            $userInstance->update();
            
            $mail=$userInstance->username=="admin"?$this->context->constantRepository->findByConstantID('adminEmail')->constant:$userInstance->username;
            $this->sendMail('registrationMail', $this->link('//Sign:newPassword', array('token'=>$token)), $mail);

        } catch (Exception $e) {
            $this->flashMessage($this->translate('Unexpected error!'));
            $this->redirect('default');
        }      

        $this->flashMessage($this->translate('User').' '.$userInstance->username.' '.$this->translate('was activated and email was sent.'));
        $this->restoreRequest($backlink);
        $this->redirect('Users:list');
    }
    
    /**
     * Set user->state = 1 and send email
     * @param $id
     */
    public function actionBann($backlink = '') {
        $this->isLogged(1);
        $this->isAdmin(1);
        $id = explode(', ', $this->getParam('id'));
        if(is_array($id)){
            $ok = null;
            $notFound = null;
            $err = null;
            
            foreach ($id as $idIn) {
               $userInstance = $this->userRepository->findById($idIn);
               if ($userInstance) {
                    try {
                        $userInstance->state=1;
                        $userInstance->update();
                        if($ok==null) $ok = $userInstance->username; 
                        else $ok=$ok.', '.$userInstance->username;
                    } catch (Exception $e) {
                        if($err==null) $err = $userInstance->username; 
                        $err=$err.', '.$userInstance->username;
                    }
               } else {
                   if($notFound==null) $notFound = $idIn; 
                   $notFound=$notFound.', '.$idIn;
               } 
            }
            if($ok!=null) $this->flashMessage($this->translate ("Users $ok was banned."), "alert-success");
            if($notFound!=null) $this->flashMessage($this->translate ("Users with id $notFound was not found."), "alert-info");
            if($err!=null) $this->flashMessage($this->translate ("Users $err was not  sent to recycler do to unexpected error."), "alert-error");
            
            $this->restoreRequest($backlink);
            $this->redirect('Users:list');
        }else{
            $userInstance = $this->userRepository->findById($id);
            if (!$userInstance) {
                $this->error($this->translate('Entry was not found'));
            }

            try {
                $userInstance->state=1;
                $userInstance->update();
            } catch (Exception $e) {
                $this->flashMessage($this->translate('Unexpected error!'));
                $this->redirect('default');
            } 

            $this->flashMessage($this->translate("User $userInstance->username was banned."), "alert-success");
            $this->restoreRequest($backlink);
            $this->redirect('Users:list');
        }
    }
    
    /**
     * Set user->state = 2 and send email
     * @param $id
     */
    public function actionUnbann($backlink = '', $id = 0) {
        $this->isLogged(1);
        $this->isAdmin(1);
        $userInstance = $this->userRepository->findById($id);
        if (!$userInstance) {
            $this->error($this->translate('Entry was not found'), "alert-error");
        }
        
        try {           
            $userInstance->state=2;
            $userInstance->update();
            
            //$this->sendMail('unbannUser');
        } catch (Exception $e) {
            $this->flashMessage($this->translate('Unexpected error!'), "alert-error");
            $this->redirect('default');
        }      

        $this->flashMessage($this->translate("User $userInstance->username has his bann lifted and email was sent."), "alert-success");
        $this->restoreRequest($backlink);
        $this->redirect('Users:list');
    }
    
    /**
     * Set user->deleted to current date and state to 3
     * @param $id
     */
    public function actionDelete($backlink = ''){
        $this->isLogged(1);
        $this->isAdmin(1);
        $id = explode(', ', $this->getParam('id'));
        if(is_array($id)){
            $ok = null;
            $notFound = null;
            $err = null;
            
            foreach ($id as $idIn) {
               $userInstance = $this->userRepository->findById($idIn);
               if ($userInstance) {
                    try {
                        $userInstance->deleted=new Nette\DateTime;
                        $userInstance->state=3;
                        $userInstance->update();
                        if($ok==null) $ok = $userInstance->username; 
                        else $ok=$ok.', '.$userInstance->username;
                    } catch (Exception $e) {
                        if($err==null) $err = $userInstance->username; 
                        $err=$err.', '.$userInstance->username;
                    }
               } else {
                   if($notFound==null) $notFound = $idIn; 
                   $notFound=$notFound.', '.$idIn;
               } 
            }
            if($ok!=null) $this->flashMessage($this->translate ("Users $ok was sent to recycler."), "alert-success");
            if($notFound!=null) $this->flashMessage($this->translate ("Users with id $notFound was not found."), "alert-info");
            if($err!=null) $this->flashMessage($this->translate ("Users $err was not  sent to recycler do to unexpected error."), "alert-error");
            
            $this->redirect('Users:list');
        }else{
            $userInstance = $this->userRepository->findById($id);
            if (!$userInstance) {
                $this->error($this->translate('Entry was not found'));
            }

            try {
                $userInstance->deleted=new Nette\DateTime;
                $userInstance->state=3;
                $userInstance->update();
            } catch (Exception $e) {
                $this->flashMessage($this->translate('Unexpected error!'));
                $this->redirect('default');
            } 

            $this->flashMessage($this->translate("User $userInstance->username was removed to recycler."), "alert-success");
            $this->restoreRequest($backlink);
            $this->redirect('Users:list');
        }
    }
    
    /**
     * Set user->deleted to null
     * @param $id
     */
    public function actionUnDelete($backlink = '') {
        $this->isLogged(1);
        $this->isAdmin(1);
        $id = explode(', ', $this->getParam('id'));
        if(is_array($id)){
            $ok = null;
            $notFound = null;
            $err = null;
            
            foreach ($id as $idIn) {
               $userInstance = $this->userRepository->findById($idIn);
               if ($userInstance) {
                    try {
                        $userInstance->deleted=null;
                        $userInstance->state=2;
                        $userInstance->update();
                        if($ok==null) $ok = $userInstance->username; 
                        else $ok=$ok.', '.$userInstance->username;
                    } catch (Exception $e) {
                        if($err==null) $err = $userInstance->username; 
                        $err=$err.', '.$userInstance->username;
                    }
               } else {
                   if($notFound==null) $notFound = $idIn; 
                   $notFound=$notFound.', '.$idIn;
               } 
            }
            if($ok!=null) $this->flashMessage($this->translate ("Users $ok was retrieved from recycler."), "alert-success");
            if($notFound!=null) $this->flashMessage($this->translate ("Users with id $notFound was not found."), "alert-info");
            if($err!=null) $this->flashMessage($this->translate ("Users $err was not retrieved from recycler do to unexpected error."), "alert-error");
            
            $this->restoreRequest($backlink);
            $this->redirect('Users:list');
        }else{
            $userInstance = $this->userRepository->findById($id);
            if (!$userInstance) {
                $this->error($this->translate('Entry was not found'));
            }

            try {
                $userInstance->deleted=null;
                        $userInstance->state=2;
                $userInstance->update();
            } catch (Exception $e) {
                $this->flashMessage($this->translate('Unexpected error!'));
                $this->redirect('default');
            } 

            $this->flashMessage($this->translate("User $userInstance->username was retrieved from recycler."), "alert-success");
            $this->restoreRequest($backlink);
            $this->redirect('Users:list');
        }
    }

    /**
     * Set user->deleted na null
     * @param $id
     */
    public function actionRestore($backlink = '', $id = 0) {
        $this->isLogged(1);
        $this->isAdmin(1);
        $userInstance = $this->userRepository->findById($id);
        if (!$userInstance) {
                $this->error($this->translate('Entry was not found'));
        }
        
        try {
                $userInstance->deleted=null;
                $userInstance->update();
        } catch (Exception $e) {
                $this->flashMessage($this->translate('Unexpected error!'));
                $this->redirect('default');
        }

        $this->flashMessage($this->translate('User').' '.$userInstance->username.' '.$this->translate('was removed to recycler.'));
        $this->restoreRequest($backlink);
        $this->redirect('Users:list');
    }
    
    
    /**
     * Set user->deleted to current date and state to 3
     * @param $id
     */
    public function actionToMailString(){
        $this->isLogged(1);
        $id = explode(', ', $this->getParam('id'));
        if(is_array($id)){
            $ok = null;
            $notFound = null;
            
            foreach ($id as $idIn) {
               $userInstance = $this->userRepository->findById($idIn);
               if ($userInstance) {
                    try {
                        if($ok==null) $ok = $userInstance->username; 
                        else $ok=$ok.', '.$userInstance->username;
                    } catch (Exception $e) {
                        $this->error();
                    }
               } else {
                   if($notFound==null) $notFound = $idIn; 
                   $notFound=$notFound.', '.$idIn;
               } 
            }
            if($ok!=null) $this->flashMessage($ok, "alert-email");
            $this->redirect('Users:list');
        }
    }
    
    /**
     * Handler for registration. Call form factory.
     */
    public function renderRegistration() {
        $this['registrationForm']['save'];
        return;
    }
    
    /**
     * Handler for editing users own account.
     */
    public function actionEditProfile(){    
        $this->isLogged(1);
        $this->redirect('Users:edit',$this->getUser()->id);
    }
    
    /**
     * Handler for editing accounts.
     * @param array $id'
     */
    public function renderEdit($id = 0) {
        $form = $this['registrationForm'];
        if (!$form->isSubmitted()) {
            $userInstance = $this->userRepository->findById($id);
            if (!$userInstance) {
                $this->error($this->translate('Entry was not found'));
            }
            $form['save']->caption = 'Save';
            $form->setDefaults($userInstance);
            $form->setDefaults(array(
                'memberSince' => $userInstance->memberSince->format('m.Y'),
                'alumniSince' => $userInstance->alumniSince->format('m.Y'),
            ));
            if($userInstance->alumniSince=='-0001-11-30 00:00:00'){
                $form->setDefaults(array(
                'alumniSince' => "",
            ));               
            }
            if($userInstance->allowedCV==1){
                $form->setDefaults(array(
                    'cv' => "true",
                ));               
            }
        }
    }
    
    /**
     * Factory for Registration/Editing form
     */
    protected function createComponentRegistrationForm(){
        $form = new Form;
        $form->setTranslator($this->translator);
        
        $form->addText('name', 'Name:',50)
                ->setAttribute("required", "true")
                ->setRequired('Name must be filled in!');
        
        $form->addText('surname', 'Surname:',50)
                ->setAttribute("required", "true")
                ->setRequired('Surname must be filled in!');
        
        $form->addText('yearOfBirth', 'Year of birth:', 50, 4)
                ->setAttribute("required", "true")
                ->setRequired('Year of birth must be filled in!')
                ->addRule(Form::PATTERN, 'Only number allowed', '[0-9]*');
        
        $form->addText('username', 'Email (login):',80)
                ->setAttribute("required", "true")
                ->setAttribute("placeholder", $this->translate("e.g. jan.novak@seznam.cz"))
                ->setRequired('Email was not filled in!')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::EMAIL, 'Wrong email format!');
        
        $form->addText('telephone', 'Telephone:', 20);
        
        $form->addSelect('lc_id', 'Local Centrum:', $this->lcRepository->getArray('lcName'))
                ->setPrompt('Choose LC')
                ->setAttribute("required", "true")
                ->setRequired('LC must be choosen!');
        
        $form->addText('memberSince', 'Member of IAESTE since:')
                ->setAttribute("placeholder", "MM.yyyy")
                ->setAttribute("required", "true")
                ->setRequired('Begining of membership must be filled in!')
                ->addRule(Form::PATTERN, 'Must be in format MM.yyyy', '[0-9][0-9][.][0-9][0-9][0-9][0-9]');
        
        $form->addText('alumniSince', 'Alumni since:')
                ->setAttribute("placeholder", "MM.yyyy")
                ->addCondition(Form::FILLED)
                    ->addRule(Form::PATTERN, 'Must be in format MM.yyyy', '[0-9][0-9][.][0-9][0-9][0-9][0-9]');
        
        //$absoluteUrlFilter = function($url) {
        //    return (Strings::startsWith($url, 'http://') || Strings::startsWith($url, 'https://')) ? $url : "http://$url";
        //};
        
        $form->addText('facebook', 'Facebook', 80)
                //->addFilter($absoluteUrlFilter)
                ->addCondition(Form::FILLED)
                    ->addRule(Form::URL, 'Facebook address must be in format www.example.com');
        $form->addText('twitter', 'Twitter', 80)
                //->addFilter($absoluteUrlFilter)
                ->addCondition(Form::FILLED)
                    ->addRule(Form::URL, 'Twitter address must be in format www.example.com');
        $form->addText('linkedin', 'LinkedIn', 80)
                //->addFilter($absoluteUrlFilter)
                ->addCondition(Form::FILLED)
                    ->addRule(Form::URL, 'LinkedIn address must be in format www.example.com');
        
        $form->addTextArea('bio', 'IAESTE BIO', 2000)
                ->setAttribute('kind', 'texyla');
        
        if($this->getUser()->isInRole('admin')){
            $form->addCheckbox('cv','Allow CV:');
            
            $form->addSelect('position_id', 'IAESTE posotion:', $this->positionRepository->getArray('positionName'))
                    ->setPrompt('Choose position')
                    ->setAttribute("required", "true")
                    ->setRequired('Posotion must be choosen!');
        }
        
        $form->addSubmit('save', 'Register')
                ->setAttribute('kind', 'mainButtonFar')         
                ->onClick[] = $this->memberFormSucceeded;
        
        if($this->isLogged(0)){
            $form->addSubmit('cancel', 'Return to users list')
                ->setValidationScope(NULL)
                ->onClick[] = $this->formCancelled;
        }else{
            $form->addSubmit('cancel', 'Return to about')
                ->setValidationScope(NULL)
                ->onClick[] = $this->formCancelled;
        }
        

        $form->addProtection();
        return $form;
    }

    /**
     * Handler for form canceling.
     */
    public function formCancelled(){
        if($this->isLogged(0)) $this->redirect('Users:list');
        $this->redirect('Homepage:about');
    }
    
    /**
     * Handler for inserting/editing users in database.
     * @param array $button'
     */
    public function memberFormSucceeded($button){
        $values = $button->getForm()->getValues();
        $id = (int) $this->getParameter('id');
        
        if ($id) { //EDITING
            $this->isLogged(1);
            try {
                $userInstance = $this->userRepository->findById($id);
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Entry was not found'));
            }
            
            
            if($userInstance->username != $values->username && $this->userRepository->didUsernameExists($values->username)){
                $button->getForm()->addError($this->translate('This email is already used!', 'alert-error'));
                return;
            }
            
            try {
                $timeH = new Nette\DateTime();
                $userInstance->name=$values->name;
                $userInstance->surname=$values->surname;
                $userInstance->yearOfBirth=$values->yearOfBirth;
                $userInstance->username=$values->username;
                $userInstance->telephone=$values->telephone;
                $userInstance->memberSince=$timeH->createFromFormat('d.m.Y', '1.'.$values->memberSince);
                $userInstance->alumniSince=$timeH->createFromFormat('d.m.Y', '1.'.$values->alumniSince);
                $userInstance->facebook=$values->facebook;
                $userInstance->twitter=$values->twitter;
                $userInstance->linkedin=$values->linkedin;
                $userInstance->lc_id=$values->lc_id;
                $userInstance->bio=$values->bio;
                $userInstance->allowedCV = $values->cv?1:0;
                if($this->isAdmin()){
                    $userInstance->position_id=$values->position_id;
                }
                $userInstance->update();
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error'), 'alert-error');
                $this->redirect('default');
            }

            $this->flashMessage($this->translate("Changes in $userInstance->username was successfully saved."), 'alert-success');
        } else { //CREATING
            //is username unique
            if($this->userRepository->didUsernameExists($values->username)){
                $button->getForm()->addError($this->translate('This email is already used!', 'alert-error'));
                return;
            }
            
            try {
                $time = new Nette\DateTime();
                $time->add(new DateInterval('PT12H'));
                $token = \Todo\Authenticator::calculateToken();
                $timeH = new Nette\DateTime();

                $userInstance = $this->userRepository->insert(array(
                        'name' => $values->name,
                        'surname' => $values->surname,
                        'yearOfBirth' => $values->yearOfBirth,
                        'username' => $values->username,
                        'telephone' => $values->telephone,
                        'memberSince' => $timeH->createFromFormat('d.m.Y', '1.'.$values->memberSince),
                        'alumniSince' => $timeH->createFromFormat('d.m.Y', '1.'.$values->alumniSince),
                        'facebook' => $values->facebook,
                        'twitter' => $values->twitter,
                        'linkedin' => $values->linkedin,
                        'lc_id' => $values->lc_id,
                        'bio' => $values->bio,
                        'position_id' => $this->isAdmin()?$values->position_id:'1',
                        'password' => \Todo\Authenticator::calculateHash(uniqid()),
                        'token' => $this->getUser()->isInRole('admin')?$token:null,
                        'tokenExp' => $this->isAdmin()?$time:null,
                        'state' => $this->isAdmin()?2:0,
                        'allowedCV' => $this->isAdmin()?$values->cv?1:0:0
                ));

                if($this->isAdmin(0)){
                    $mail=$userInstance->username=="admin"?$this->context->constantRepository->findByConstantID('adminEmail')->constant:$userInstance->username;
                    $this->sendMail('registrationMail', $this->link('//Sign:newPassword', array('token'=>$token)),$mail);
                }elseif ($this->isLogged(0)){
                    $this->sendMail('newUserMail');
                }else{
                    $mail=$userInstance->username=="admin"?$this->context->constantRepository->findByConstantID('adminEmail')->constant:$userInstance->username;
                    
                    $this->sendMail('selfRegistrationMail',"", $mail);
                    $this->sendMail('newUserMail');
                }
                $this->flashMessage($this->translate("User $userInstance->username was registreted."), 'alert-success');
            } catch (Exception $exc) {
                $this->flashMessage($this->translate('Unexpected error'), 'alert-error');
                $this->redirect('Homepage:about');
            }
        }   
        
        if($this->isLogged(0)) $this->redirect ('Users:list');
        $this->redirect('Homepage:about');  
    }
    
    
    
    /**
     * Handler for editing accounts.
     */
    public function renderFotoUpload() {
        $this->template->userInstance = "pokus"; 
    }
    
    /**
     * Factory for Foto upload form
     */
    protected function createComponentPhotoUploadForm(){
        $form = new Form;
        $form->setTranslator($this->translator);
        $form->addUpload('photo', 'Photo:')
                ->setAttribute("required", "true")
                ->setRequired('You must choose a photo!')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::IMAGE, 'Photo myst be in JPEG, PNG or GIF format.')
                        ->addRule(Form::MAX_FILE_SIZE, 'Max size of photi is 64 kB.', 64 * 1024 /* v bytech */);
        
        $form->addSubmit('save', 'Upload pthoto')
                ->setAttribute('kind', 'mainButtonFar')         
                ->onClick[] = $this->photoUploadFormSucceeded;
        
        $form->addProtection();
        return $form;
    }
    
    /**
     * Handler for updating photo
     * @param array $button'
     */
    public function photoUploadFormSucceeded($button){
        $values = $button->getForm()->getValues();
        
        $this->isLogged(1);
        try {
            $userInstance = $this->userRepository->findById($this->getUser()->getIdentity()->id);     
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Entry was not found'));
            $this->redirect('Homepage:about');
        }

        $image = Image::fromFile($values->photo);
        /*if($image->width!=$image->height){
            $button->getForm()->addError($this->translate('Width and height are not same!'));
            return;
        }

        if($image->getWidth()<128 || $image->getWidth()>300){
            $button->getForm()->addError($this->transalate('Width and height must be betveen 128 and 300 px!'));
            return;
        }*/
        
        $imageName =sha1(uniqid());
        $image->resize(128, 128, Image::SHRINK_ONLY | Image::STRETCH);
        if($userInstance->photo) $image->save($userInstance->photo);
        else $image->save('avatars/'.$imageName.'.jpg');

        try {
            $userInstance->photo=$imageName;
            $userInstance->update();
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Unexpected error'), 'alert-error');
            $this->redirect('default');
        }

        $this->flashMessage($this->translate("Photo was successfully uploaded."), 'alert-success');      
        $this->redirect('Homepage:about');  
    }
    
}