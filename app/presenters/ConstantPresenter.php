<?php
use Nette\Application\UI\Form;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Forms\Container;
/**
 * Description of ConstantPresenter
 *
 * @author tomasprix
 */
class ConstantPresenter extends BasePresenter {
    private $constantRepository; 

    /**
     * (non-phpDoc)
     *
     * @see Nette\Application\Presenter#startup()
     */
    protected function startup() {
        parent::startup();
        $this->isLogged(1);
        $this->isAdmin(1);
        
        $this->constantRepository = $this->context->constantRepository;
    }

    public function renderEditAdminEmail() {
        $form = $this['emailForm'];
        if (!$form->isSubmitted()) {
            $constantInstance = $this->constantRepository->findByConstantID('adminEmail');
            if (!$constantInstance) {
                $this->error($this->translate('Row was not found.', 'alert-error'));
            }
            $form->setDefaults($constantInstance);
        }
    }
    
    /**
     * Call form for editing Admin Email 
     */
    protected function createComponentEmailForm(){
        $form = new Form;
        $form->addText('constant', 'Email:', 30, 80)
                ->setAttribute("length", "longer")
                ->setRequired('Email wasnt filled!')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::EMAIL, 'Wrong email format!');
        $form->addSubmit('save', 'Save')
                ->setAttribute('kind', 'mainButtonFar')         
                ->onClick[] = $this->emailFormSucceeded;
        return $form;
    }
    
    /**
     * Handler for editing text in database.
     * @param array $button'
     */
    public function emailFormSucceeded($button){
        $values = $button->getForm()->getValues();

        try {
            $constantInstance = $this->constantRepository->findByConstantID('adminEmail');
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
        }

        try {
            $constantInstance->constant=$values->constant;
            $constantInstance->update();
            $this->flashMessage($this->translate('Changes in entry ').$constantInstance->constantName.$this->translate(' was successfully saved.'), 'alert-success');
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Unexpected error!', 'alert-error'));
            $this->redirect('Homepage:');
        }

        $this->redirect('Homepage:');
    }

    public function renderEditContact() {
        try {
            $constantInstance = $this->constantRepository->findByConstantID('contact');
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
        }
        
        $form = $this['contactForm'];
        if (!$form->isSubmitted()) {
            
            $rows = $this->textToArray($constantInstance->constant);
            foreach ($rows as $key => $row) {
                $form['rows'][$key]->setValues(array('row' => $row));
                # naplní kontejner výchozími hodnotami
            }
            
        }
    }
    
    /**
    * @return Nette\Application\UI\Form
    */
    public function createComponentContactForm(){
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $nodes = $form->addDynamic("rows", function (Container $container) {
             $container->addText("row", "Row")
                     ->setAttribute("length", "longer")
                     ->setAttribute("kind", "row");

             //button for removing the new node
             $container->addSubmit("removeNode", "Remove row")
                     ->setAttribute("kind", "removeRowButton")
                     ->addRemoveOnClick();
             }, 2);
        $nodes->addSubmit("addNode", "Add row")
                    ->setAttribute("kind", "addRowButton")
                     ->addCreateOnClick(TRUE);     

        $form->addSubmit('save', 'Save')         
             ->onClick[] = $this->constantFormSucceeded;
        return $form;
    }

    public function constantFormSucceeded($button){
        $values = $button->getForm()->getValues();

        //Dumping rows into string
        $contact = '';
        foreach ($values['rows'] as $key => $row) { // pracujeme s polem hodnot
           if($key==0){
               $contact=$row['row'];
           }else{
               $contact=$contact.';'.$row['row'];
           }
        }
        
        try {
            $constantInstance = $this->constantRepository->findByConstantID('contact');
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Row was not found!', 'alert-error'));
        }

        try {
            $constantInstance->constant=$contact;
            $constantInstance->update();
            $this->flashMessage($this->translate('Changes in entry ').$constantInstance->constantName.$this->translate(' was successfully saved.'), 'alert-success');
        } catch (Exception $exc) {
            $this->flashMessage($this->translate('Unexpected error!', 'alert-error'));
            $this->redirect('Homepage:');
        }
            

        $this->redirect('Homepage:');
    }
}