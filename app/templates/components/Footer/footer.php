<?php

namespace Todo;
use Nette;

class FooterControl extends Nette\Application\UI\Control{
    private $translator;

    public function __construct(\Nette\Localization\ITranslator $translator){
        parent::__construct();
        $this->translator = $translator;
    }
    
    public function setTranslator(\Nette\Localization\ITranslator $translator) {
	$this->translator = $translator;
    }

    public function render(){
        $this->template->setFile(__DIR__ . '/footer.latte');
        $this->template->setTranslator($this->translator);
        $this->template->render();
    }
}