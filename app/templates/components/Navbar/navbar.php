<?php

namespace Todo;
use Nette;

class NavbarControl extends Nette\Application\UI\Control{
    private $translator;
    private $lang;
    private $langs;


    public function __construct(\Nette\Localization\ITranslator $translator, $lang, $languageRepository){
        parent::__construct();
        $this->translator = $translator;
        $this->lang = $lang;
        $this->langs = $languageRepository;
    }
    
    public function setTranslator(\Nette\Localization\ITranslator $translator) {
	$this->translator = $translator;
    }

    public function render(){
        $this->template->setFile(__DIR__ . '/navbar.latte');
        $this->template->setTranslator($this->translator);
        $this->template->lang = $this->lang;
        $this->template->langs = $this->langs;
        $this->template->render();
    }
    
}