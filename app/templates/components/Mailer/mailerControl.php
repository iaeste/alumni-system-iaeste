<?php   
namespace Todo;
use Nette;
use Nette\Mail\Message;

class mailerControl extends Nette\Application\UI\Control{
    private $adminEmail;
    private $contact;
    
     
    public function __construct($adminEmail, $contact){
        parent::__construct(); // vždy je potřeba volat rodičovský konstruktor
        $this->adminEmail = $adminEmail;
        $this->contact = $contact;
    }
    
    /**
     * Handler for generating and sending mails
     * @param $emailInstance type of email (resetPasswordMail, registrationMail, newUserMail)
     * @param $redirect fill in if you need to send link othervise '' will be filled
     * @param $to if not filled in email will be send to administrator
     */
    public function sendMail($emailInstance, $redirect='', $to=null){  
        try {
            $template = new Nette\Templating\FileTemplate;
            $template->setFile(__DIR__ . '/email.latte');
            $template->title = $emailInstance->subject;
            $template->salut = $emailInstance->salut;
            $template->text = $emailInstance->text;
            $template->redirect = $redirect;
            $template->regards = $emailInstance->regards;
            $template->foot = $this->contact;
            $template->registerFilter(new Nette\Latte\Engine);
            $template->registerHelperLoader('Nette\Templating\Helpers::loader');

            $mail = new Message;
            $mail->setSubject($emailInstance->subject)
                ->setFrom($this->adminEmail, $this->adminEmail)
                ->addReplyTo($this->adminEmail, $this->adminEmail)
                ->addTo($to!=null?$to:$this->adminEmail)
                ->setHtmlBody($template)
                ->send();
            
            return true;
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
    
    /**
     * Handler for generating and sending user mails
     * @param $from 
     * @param $text
     * @param $subject
     */
    public function sendUserMail($from, $text, $subject){  
        try {
            $template = new Nette\Templating\FileTemplate;
            $template->setFile(__DIR__ . '/emailUser.latte');
            $template->title = $subject;
            $template->text = $text;
            $template->registerFilter(new Nette\Latte\Engine);
            $template->registerHelperLoader('Nette\Templating\Helpers::loader');

            $mail = new Message;          
            $mail->setSubject($subject)
                ->setFrom($from, $from)
                ->addReplyTo($from, $from)
                ->addTo($this->adminEmail)
                ->setHtmlBody($template)
                ->send();
            
            return true;
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
}

