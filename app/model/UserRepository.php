<?php

namespace Todo;
use Nette;

class UserRepository extends Repository{
    /**
     * Return user row from database. Look up by username. 
     * @return Nette\Database\Table\Selection
     */
    public function findByUsername($username){
        return $this->findFirstByArray(array('username' => $username));
    }
    
    /**
     * Call findByUsername and return true if user was found.
     * @return boolean
     */
    public function didUsernameExists($username){
        return $this->findByUsername($username)?true:false;
    }
    
    /**
     * Return user row from database. Look up by token. 
     * @return Nette\Database\Table\Selection
     */
    public function findByToken($token){
        return $this->findFirstByArray(array('token' => $token));
    }
    
    public function joinWithPositionAndLC(){
        return $this->join(); 
    }
    
    public function pendingUsersCount(){
        return $this->countAllBy(array('state' => 0));
    }
    
    public function findUsersWithLcID($lcID){
        return $this->findAllBy(array("lc_id" => $lcID));
    }
    
    public function numberOfUsersWithLcID($lcID){
        return $this->findUsersWithLcID($lcID)->count();
    }
    
    public function findUsersWithPositionID($positionID){
        return $this->findAllBy(array("position_id" => $positionID));
    }
    
    public function numberOfUsersWithPositionID($positionID){
        return $this->findUsersWithPositionID($positionID)->count();
    }
}

