<?php

namespace Todo;

use Nette,
    Nette\Security,
    Nette\Utils\Strings;


/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Security\IAuthenticator{
	/** @var UserRepository */
	private $users;

	public function __construct(UserRepository $users){
            $this->users = $users;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials){
            list($username, $password) = $credentials;
            $row = $this->users->findByUsername($username);

            //user exists?
            if (!$row) {
                throw new Security\AuthenticationException('User does not exist!', self::IDENTITY_NOT_FOUND);
            }

            //user activated?
            if ($row->state == 0){
                throw new Security\AuthenticationException('User is not activated!', self::NOT_APPROVED);
            }
            
            //user banned?
            if ($row->state == 1){
                throw new Security\AuthenticationException('User does is banned!', self::NOT_APPROVED);
            }
            
            //user deleted?
            if ($row->state == 3){
                throw new Security\AuthenticationException('User was deleted!', self::NOT_APPROVED);
            }
            
            //right password?
            if ($row->password !== $this->calculateHash($password, $row->password)) {
                throw new Security\AuthenticationException('Wrong password!', self::INVALID_CREDENTIAL);
            }
            
            //save login time
            $row->lastLogged = new Nette\DateTime();
            $row->update();
            
            unset($row->password);
            
            return new Security\Identity($row->id, $username=='admin'?'admin':'user', $row->toArray());
	}

	/**
         * Set new password
	 * @param  int $id
	 * @param  string $password
	 */
	public function setPassword($id, $password){
            $this->users->findBy(array('id' => $id))->update(array(
                    'password' => $this->calculateHash($password),
            ));
	}
        

        /**
	 * Computes salted password hash.
	 * @param string
	 * @return string
	 */
	public static function calculateHash($password, $salt = NULL){
            if ($password === Strings::upper($password)) { // perhaps caps lock is on
                    $password = Strings::lower($password);
            }
            return crypt($password, $salt ?: '$2a$07$' . Strings::random(22));
	}

        /**
         * Computes unique token.
         * @return string
         */
        public static function calculateToken(){
            return sha1(uniqid());
        }
}