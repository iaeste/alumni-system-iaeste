<?php

namespace Todo;
use Nette;

class ConstantRepository extends Repository{
    public function findByConstantID($constantID){
        return $this->findFirstByArray(array('constantID' => $constantID));
    }
}

