<?php

namespace Todo;
use Nette;

class MailRepository extends Repository{
    public function findByMailID($mailID){
        return $this->findFirstByArray(array('mailID' => $mailID));
    }
}

