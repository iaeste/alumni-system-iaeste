<?php

namespace Todo;
use Nette;

/**
 * Operations above database table.
 */
abstract class Repository extends Nette\Object{
    /** @var Nette\Database\Connection */
    protected $connection;

    public function __construct(Nette\Database\Connection $db){
        $this->connection = $db;
    }

    /**
     * Return object representig database table.
     * @return Nette\Database\Table\Selection
     */
    protected function getTable(){
        // název tabulky odvodíme z názvu třídy
        preg_match('#(\w+)Repository$#', get_class($this), $m);
        return $this->connection->table(lcfirst($m[1]));
    }

    /**
     * Return all rows from table.
     * @return Nette\Database\Table\Selection
     */
    public function findAll(){
        return $this->getTable();
    }

    /**
     * Return first row by filter, exmp. array('name' => 'John').
     * @return Nette\Database\Table\Selection
     */
    public function findFirstByArray(array $by){
        return $this->getTable()->where($by)->fetch();
    }
    
    /**
     * Return rows by filter, exmp. array('name' => 'John').
     * @return Nette\Database\Table\Selection
     */
    public function findAllBy(array $by){
        return $this->getTable()->where($by);
    }
    
    public function countAllBy(array $by){
        return $this->findAllBy($by)->count();
    }
    
    public function findById($id){
        return $this->findAll()->get($id);
    }
    
    public function insert($values){
        return $this->findAll()->insert($values);
    }
    
    public function delete($id){
        try {
            $this->findById($id)->delete();
            return true;
        } catch (Exception $e) {
            return false;
        } 
    }
    
    public function countAll(){
        return $this->findAll()->count();
    }


    public function getArray($colName){
        return $this->getTable()->order($colName)->fetchPairs('id', $colName);
    }

    public function join(){
        return $this->findAll()->select("user.name, user.surname, user.username, user.memberSince, user.alumniSince, user.allowedCV, user.state, lc.lcName, position.positionName, user.id")
                ->where(array("username <> ?"=>"admin"));
    }
}