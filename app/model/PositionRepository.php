<?php

namespace Todo;
use Nette;

class PositionRepository extends Repository{
    public function findByPositionID($positionID){
        return $this->findFirstByArray(array('positionID' => $positionID));
    }
    
}

