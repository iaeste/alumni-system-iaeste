<?php

namespace Todo;
use Nette;

class TextRepository extends Repository{
    public function findByTextID($textID){
        return $this->findFirstByArray(array('textID' => $textID));
    }
}

